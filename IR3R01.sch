EESchema Schematic File Version 2
LIBS:JP8-voice-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-voice-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 36 50
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 7050 3225 0    60   Input ~ 0
1-mod
Text HLabel 7050 3325 0    60   Input ~ 0
2-a
Text HLabel 7050 3425 0    60   Input ~ 0
3-d
Text HLabel 7050 3525 0    60   Input ~ 0
4-r
Text HLabel 7050 3625 0    60   Input ~ 0
5-s
Text HLabel 7050 3725 0    60   Input ~ 0
6-com
Text HLabel 7050 3825 0    60   Input ~ 0
7-ref
Text HLabel 7050 3925 0    60   Input ~ 0
8-v-
Text HLabel 7750 3925 2    60   Input ~ 0
9-gnd
Text HLabel 7750 3825 2    60   Input ~ 0
10-cap
Text HLabel 7750 3725 2    60   Input ~ 0
11-out
Text HLabel 7750 3625 2    60   Input ~ 0
12-cur-in
Text HLabel 7750 3525 2    60   Input ~ 0
13-gate
Text HLabel 7750 3425 2    60   Input ~ 0
14-trig
Text HLabel 7750 3325 2    60   Input ~ 0
15-ret
Text HLabel 7750 3225 2    60   Input ~ 0
16-v+
$Comp
L DIL16 IC36
U 1 1 5BEFDAA5
P 7400 3575
AR Path="/5BE55319/5BFAD945/5BEFDAA5" Ref="IC36"  Part="1" 
AR Path="/5BE545E0/5BEEBEFE/5BEFDAA5" Ref="IC39"  Part="1" 
F 0 "IC39" H 7400 4025 50  0000 C CNN
F 1 "IR3R01" V 7400 3575 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm" H 7400 3575 50  0001 C CNN
F 3 "" H 7400 3575 50  0001 C CNN
	1    7400 3575
	1    0    0    -1  
$EndComp
$EndSCHEMATC
