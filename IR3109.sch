EESchema Schematic File Version 2
LIBS:JP8-voice-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-voice-cache
EELAYER 25 0
EELAYER END
$Descr A1 33110 23386
encoding utf-8
Sheet 26 50
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 10475 9975 2    60   Input ~ 0
16-v+
Text HLabel 10475 10075 2    60   Input ~ 0
15-out-4
Text HLabel 10475 10175 2    60   Input ~ 0
14-c-4
Text HLabel 10475 10275 2    60   Input ~ 0
13-in-4
Text HLabel 9775 9975 0    60   Input ~ 0
1-gnd
Text HLabel 10475 10375 2    60   Input ~ 0
12-out-3
Text HLabel 10475 10475 2    60   Input ~ 0
11-c-3
Text HLabel 10475 10575 2    60   Input ~ 0
10-in-3
Text HLabel 10475 10675 2    60   Input ~ 0
9-control-in
Text HLabel 9775 10675 0    60   Input ~ 0
8-v-
Text HLabel 9775 10575 0    60   Input ~ 0
7-out-2
Text HLabel 9775 10475 0    60   Input ~ 0
6-c-2
Text HLabel 9775 10375 0    60   Input ~ 0
5-in-2
Text HLabel 9775 10275 0    60   Input ~ 0
4-out-1
Text HLabel 9775 10175 0    60   Input ~ 0
3-c-1
Text HLabel 9775 10075 0    60   Input ~ 0
2-in-1
$Comp
L DIL16 IC27
U 1 1 5BF00655
P 10125 10325
F 0 "IC27" H 10125 10775 50  0000 C CNN
F 1 "IR3109" V 10125 10325 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm" H 10125 10325 50  0001 C CNN
F 3 "" H 10125 10325 50  0001 C CNN
	1    10125 10325
	1    0    0    -1  
$EndComp
$EndSCHEMATC
