EESchema Schematic File Version 2
LIBS:JP8-voice-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-voice-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 26 50
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5700 3550 1275 800 
U 5BE63338
F0 "vca_mod_ba662" 60
F1 "ba662.sch" 60
F2 "1" I L 5700 3625 60 
F3 "2" I L 5700 3700 60 
F4 "3" I L 5700 3775 60 
F5 "4" I L 5700 3850 60 
F6 "5" I L 5700 3925 60 
F7 "6" I L 5700 4000 60 
F8 "7" I L 5700 4075 60 
F9 "8" I L 5700 4150 60 
F10 "9" I L 5700 4225 60 
$EndSheet
NoConn ~ 5700 4150
Text GLabel 5425 4225 0    60   Input ~ 0
+15v
Text GLabel 5425 3850 0    60   Input ~ 0
-15v
Wire Wire Line
	5425 3850 5700 3850
Wire Wire Line
	5550 3850 5550 3925
Wire Wire Line
	5550 3925 5700 3925
Connection ~ 5550 3850
Text HLabel 5700 3625 0    60   Input ~ 0
1
Text HLabel 5700 3700 0    60   Input ~ 0
2
Text HLabel 5700 3775 0    60   Input ~ 0
3
Text HLabel 5700 4000 0    60   Input ~ 0
6
Wire Wire Line
	5425 4225 5700 4225
Wire Wire Line
	5700 4075 5550 4075
Wire Wire Line
	5550 4075 5550 4225
Connection ~ 5550 4225
$EndSCHEMATC
